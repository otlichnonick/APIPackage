//
//  BasePresenter.swift
//  SecondHomeworkApp
//
//  Created by Anton Agafonov on 08.07.2022.
//

import Foundation
import Combine

public class APIPresenter {
    private var bag: Set<AnyCancellable> = .init()
    
    public init() {}
    
    public func baseRequest<T: Codable>(publisher: AnyPublisher<T, Error>, handler: @escaping (Result<T, String>) -> Void) {
        publisher
            .subscribe(on: DispatchQueue.global())
            .receive(on: DispatchQueue.main)
            .mapError { error -> Error in
                handler(.failure(error.localizedDescription))
                return error
            }
            .sink { _ in } receiveValue: { response in
                handler(.success(response))
            }
            .store(in: &bag)
    }
}

extension String: Error {}

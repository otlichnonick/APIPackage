//
//  APIManager.swift
//  SecondHomeworkApp
//
//  Created by Anton Agafonov on 07.07.2022.
//

import Foundation
import Combine

public protocol APIManager {
    var baseURL: String { get }
}

extension APIManager {
    public func fetch<Output: Codable>(endpoint: APICall, decoder: JSONDecoder = JSONDecoder()) -> AnyPublisher<Output, Error> {
          do {
              let request = try endpoint.urlRequest(baseUrl: baseURL)
              return URLSession.shared.dataTaskPublisher(for: request)
                  .tryMap { result -> Output in
                      let httpResponse = result.response as? HTTPURLResponse
                      NetworkLogger.log(response: httpResponse, data: result.data)
                      
                      if httpResponse?.statusCode == 204 {
                          throw ErrorHandler.noContent
                      }
          
                      return try ErrorHandler.checkDecodingErrors(decoder: decoder, model: Output.self, with: result.data)
                  }
                  .eraseToAnyPublisher()
          } catch {
              return AnyPublisher(
                  Fail<Output, Error>(error: ErrorHandler.notValidURL)
              )
          }
      }
      
      public func fetch<Input: Codable, Output: Codable>(endpoint: APICall, params: Input? = nil, decoder: JSONDecoder = JSONDecoder()) -> AnyPublisher<Output, Error> {
          do {
              let request = try endpoint.urlRequest(baseURL: baseURL, bodyData: params)
              return URLSession.shared.dataTaskPublisher(for: request)
                  .tryMap { result -> Output in
                      let httpResponse = result.response as? HTTPURLResponse
                      NetworkLogger.log(response: httpResponse, data: result.data)
                      
                      if httpResponse?.statusCode == 204 {
                          throw ErrorHandler.noContent
                      }
                      
                      return try ErrorHandler.checkDecodingErrors(decoder: decoder, model: Output.self, with: result.data)
                  }
                  .eraseToAnyPublisher()
          } catch {
              return AnyPublisher(
                  Fail<Output, Error>(error: ErrorHandler.notValidURL)
              )
          }
      }
  }
